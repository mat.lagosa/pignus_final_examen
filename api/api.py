from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import UserSerializer

class UserAPI(APIView):
    def post(self, request):
        serializers = UserSerializer( data = request.data)
        if serializers.is_valid():
            user = serializers.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
    
