
from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views
from .views import PostCreateView, PostListView, PostDetailView, PostUpdateView, PostDeleteView
from api.api import UserAPI

urlpatterns = [
  path('',views.index,name="index"),
  path('perfil/', views.profile, name='profile'),
  path('somos/',views.somos,name="somos"),
  path('contactenos/',views.contactenos,name="contactenos"),
  path('register/', views.register, name='register'),
  path('administrar/', PostListView.as_view(), name = 'admin'), 
  path('perfil/edit/', views.edit, name = 'edit'),
  path('post/', PostCreateView.as_view(), name='post-create'),
  path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
  path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
  path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
  path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'),name='logout'),
  path('login/', auth_views.LoginView.as_view(template_name='login.html'),name='login'),
  path('password-reset/', auth_views.PasswordResetView.as_view(template_name='password_reset.html'), name='password_reset'),
  path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='password_reset_done'),
  path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'), name='password_reset_confirm'),
  path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
  path('api/1.0/create_user/', UserAPI.as_view(), name='api_create_user'),                                                                                                                        
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)