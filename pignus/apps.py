from django.apps import AppConfig


class PignusConfig(AppConfig):
    name = 'pignus'

    def ready(self):
        import pignus.signals
